package pl.kamil;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "book")
public class Book {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int idbook;

    @Column(name = "name")
    private String name;

    @Column(name = "year")
    private int year;

//    private List<Author> authors;

}
