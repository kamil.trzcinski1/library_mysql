package pl.kamil;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;
import java.util.List;

public class JPAconnect {
    private static Persistence Presistance;

    public static void main(String[] args) {
        EntityManager em = Presistance.createEntityManagerFactory("z1il0j7x0uezmvot").createEntityManager();
//        Author author = new Author();
//        author.setName("Brent");
//        author.setSurname("Weeks");

        em.getTransaction().begin();

//        try {
//            em.persist(author);
//            em.getTransaction().commit();
//        } catch (Exception e){
//            em.getTransaction().rollback();
//        }

        TypedQuery<Author> q = em.createQuery("SELECT u FROM Author u", Author.class);
        List<Author> res = q.getResultList();
        for(Author author:res){
            System.out.println(author);
        }
    }
}
