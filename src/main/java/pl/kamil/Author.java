package pl.kamil;

import javax.persistence.*;

@Entity
@Table(name = "author")
public class Author {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int idauthor;

    @Column(name = "name")
    private String name;

    @Column(name = "surname")
    private String surname;

    public Author() {
    }

    ;

    public int getId() {
        return idauthor;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    @Override
    public String toString() {
        return idauthor + " " + name + " " + surname;
    }
}

